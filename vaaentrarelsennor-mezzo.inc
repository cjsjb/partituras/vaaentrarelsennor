\context Staff = "mezzosoprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzosoprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key d \major

		R1*2  |
		fis' 4 e' d' fis'  |
		e' 2 ( b )  |
%% 5
		e' 8 e' 4 e' 8 cis' 4 b  |
		d' 4 ( e' ) fis' 2  |
		e' 4. e' 8 cis' 4. d' 8  |
		d' 2. r8 a  |
		cis' 8 cis' 4 d' 8 e' 4. a 8  |
%% 10
		d' 8 d' 4 e' 8 fis' fis' 4 a 8  |
		e' 8 e' e' fis' g' 8. fis' e' 8  |
		fis' 4 ( g' ) a' 4. r8  |
		g' 8 g' 4 fis' 8 e' 2  |
		fis' 8 fis' 4 e' 8 d' d' 4 r8  |
%% 15
		e' 8 e' 4 e' 8 e' 4. r8  |
		\time 2/4
		e' 8 e' 4 e' 8  |
		\time 4/4
		cis' 4 cis' 2. ~  |
		cis' 2. r4  |
		fis' 4 e' d' fis'  |
%% 20
		e' 2 ( b )  |
		e' 8 e' 4 e' 8 cis' 4 b  |
		d' 4 ( e' ) fis' 2  |
		e' 4. e' 8 b 4. d' 8  |
		d' 2. r4  |
%% 25
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzosoprano" {
		"Va a en" -- trar el Se -- ñor, __
		él es el Rey de glo -- ria,
		el Se -- ñor es Rey.

		La tie -- "rra es" de Dios y cuan -- to la lle -- na
		el or -- "be y" to -- dos sus ha -- bi -- tan -- tes;
		él la fun -- dó so -- bre los ma -- res,
		él "la a" -- fian -- zó so -- bre los rí -- os. __

		"Va a en" -- trar el Se -- ñor, __
		él es el Rey de glo -- ria,
		el Se -- ñor es Rey.
	}
>>
