\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key d \major

		R1*2  |
		a' 4 g' fis' a'  |
		g' 2 ( d' )  |
%% 5
		g' 8 g' 4 fis' 8 e' 4 d'  |
		d' 4 ( e' ) fis' 2  |
		g' 4. fis' 8 e' 4. fis' 8  |
		fis' 2. r8 a  |
		cis' 8 cis' 4 d' 8 e' 4. a 8  |
%% 10
		d' 8 d' 4 e' 8 fis' fis' 4 a 8  |
		e' 8 e' e' fis' g' 8. fis' e' 8  |
		fis' 4 ( g' ) a' 4. r8  |
		b' 8 b' 4 a' 8 g' 2  |
		a' 8 a' 4 g' 8 fis' fis' 4 r8  |
%% 15
		e' 8 e' 4 e' 8 e' 4. r8  |
		\time 2/4
		e' 8 e' 4 e' 8  |
		\time 4/4
		a' 4 a' 2. ~  |
		a' 2. r4  |
		a' 4 g' fis' a'  |
%% 20
		g' 2 ( d' )  |
		g' 8 g' 4 fis' 8 e' 4 d'  |
		d' 4 ( e' ) fis' 2  |
		g' 4. fis' 8 e' 4. fis' 8  |
		fis' 2. r4  |
%% 25
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		"Va a en" -- trar el Se -- ñor, __
		él es el Rey de glo -- ria,
		el Se -- ñor es Rey.

		La tie -- "rra es" de Dios y cuan -- to la lle -- na
		el or -- "be y" to -- dos sus ha -- bi -- tan -- tes;
		él la fun -- dó so -- bre los ma -- res,
		él "la a" -- fian -- zó so -- bre los rí -- os. __

		"Va a en" -- trar el Se -- ñor, __
		él es el Rey de glo -- ria,
		el Se -- ñor es Rey.
	}
>>
