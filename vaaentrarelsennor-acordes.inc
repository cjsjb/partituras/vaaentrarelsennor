\context ChordNames
	\chords {
	\set majorSevenSymbol = \markup { "maj7" }
	\set chordChanges = ##t

	% intro
	d1 d1

	% va a entrar el sennor...
	d1 g1 e2:m a2 d2 b2:m
	e2:m a2 d1

	% la tierra es de dios...
	a1 d1 a1 d2 d2:7
	g2 a2 d2 b2:m
	e2:m g2 e2:m
	a1 a1

	% va a entrar el sennor...
	d1 g1 e2:m a2 d2 b2:m
	e2:m a2 d1
	}
